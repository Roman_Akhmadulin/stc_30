package HomeWork2; // Вывод среднеарифметического числа массива.

import java.util.Scanner;

public class task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int [] a = new int[scanner.nextInt()];
        double arrayAverage = 0;

        for (int i = 0; i < a.length; i++){
            a[i] = scanner.nextInt();
        }

        for (int i = 0; i < a.length; i++){
            arrayAverage += a[i];
        }

        arrayAverage = arrayAverage / a.length;
        System.out.print(arrayAverage);
    }
}
