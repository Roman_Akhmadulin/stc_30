package HomeWork2; // Вывод суммы элементов массива.

import java.util.Scanner;

public class task1 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int [] a = new int [scanner.nextInt()];
        int arraySum = 0;

        for (int i = 0; i < a.length; i++){
            a[i] = scanner.nextInt();
        }

        for (int i = 0; i < a.length; i++){
            arraySum = arraySum + a[i];
        }
        System.out.println(arraySum);
    }
}
