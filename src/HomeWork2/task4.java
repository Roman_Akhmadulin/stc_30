package HomeWork2; // Перестановка максимального и минимального элемента массива.

import java.util.Arrays;
import java.util.Scanner;

public class task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int [] a = new int[scanner.nextInt()];

        for (int i = 0; i < a.length; i++){
            a[i] = scanner.nextInt();
        }
        System.out.println(Arrays.toString(a));// Вывод введённого массива
        int minIndex = 0;
        int maxIndex = 0;
        // Поиск минимального и максимального значения в массиве
        for (int i = 1; i < a.length-1; i++){
            if(a[i]<minIndex) minIndex = a[i];
            if(a[i]>maxIndex) maxIndex = a[i];
        }
        //System.out.println("minIndex: " + minIndex + "; maxIndex: " + maxIndex);// Вывод минимального и максимального значения в массиве

        int temp = minIndex; // перестановка минимального индекса с максимальным
        minIndex = maxIndex;
        maxIndex = temp;

        System.out.println("minIndex: " + minIndex + "; maxIndex: " + maxIndex);
    }
}
