package HomeWork2; // Разворот массива.

import java.util.Arrays;
import java.util.Scanner;

public class task2 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int [] n = new int[scanner.nextInt()];

        for (int i = 0; i < n.length; i++){
            n[i] = scanner.nextInt();
        }
        //System.out.println(Arrays.toString(n)); // Вывод массива на экран.

        for (int i = 0; i < n.length / 2; i++){
            int b = n[i];
            n[i] = n[n.length - 1 - i];
            n[n.length - i - 1] = b;
        }
        System.out.println(Arrays.toString(n));
    }
}
