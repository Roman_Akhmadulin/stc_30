package HomeWork2;

import java.util.Arrays;
import java.util.Scanner;

public class task5 {

        public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);
            int[] a = new int[scanner.nextInt()];

            for (int i = 0; i < a.length; i++){
                a[i] = scanner.nextInt();
            }

            System.out.println(Arrays.toString(a)); // Вывод введённого массива
// Сравниваем элементы попарно
            for (int i = a.length - 1; i > 0; i--){
                for (int j = 0; j < i; j++){
                    if (a[j] > a[j+1]){ // если они имеют неправильный порядок
                        int temp = a[j]; // то меняем местами
                        a[j] = a[j+1];
                        a[j+1] = temp;
                    }
                }
            }
            System.out.println(Arrays.toString(a)); // Вывод отсортированного массива
        }
    }
