package HomeWork1; // Вывод суммы цифр пятизначного числа.

public class task1 {

    public static void main(String[] args) {
        int number = 12345;
        int result = 0;
        result += number % 10;
        number /= 10;
        result += number % 10;
        number /= 10;
        result += number % 10;
        number /= 10;
        result += number % 10;
        number /= 10;
        result += number % 10;
        System.out.println(result);
    }
}
