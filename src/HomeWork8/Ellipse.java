package HomeWork8;

public class Ellipse extends Figure{


    public Ellipse(int r, int R){
        super(r,R);
    }

    public float getArea() {
        return (float) Math.PI * length * width*scale;
    }

    public float getPerimeter() {
        return (float) ((4 * Math.PI * length * width + Math.pow((length - width),2))/ length + width)*scale;
    }

}
