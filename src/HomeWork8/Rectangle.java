package HomeWork8;

public class Rectangle extends Figure{

    public Rectangle(int l,int w){
        super(l,w);
    }

    public float getArea(){
        float a = length * width * scale;
        return a;
    }

    public float getPerimeter(){
        return 2 * (length + width) * scale;
    }
}
