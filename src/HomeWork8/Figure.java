package HomeWork8;

public abstract class Figure implements Scalable{

    protected float scale;

    protected int length;
    protected int width;

    public Figure(int length, int width) {
        this.length = length;
        this.width = width;

        this.scale = 1;
    }

    public void setScale(float s){
       scale = s;
    }

    public abstract float getArea();

    public abstract float getPerimeter();
}
