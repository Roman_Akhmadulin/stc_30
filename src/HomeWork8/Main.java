package HomeWork8;

public class Main {

    public static void main(String[] args) {
        Rectangle rectangle1 = new Rectangle(5,8);
       // Rectangle rectangle2 = new Rectangle(2,9);

        Ellipse ellipse = new Ellipse(7,3);

        Circle circle = new Circle(6);

        float e1 =  ellipse.getArea();
        float e2 =  ellipse.getPerimeter();

        float c1 =  circle.getArea();
        float c2 =  circle.getPerimeter();

        float a1 = rectangle1.getArea();
        float p1 = rectangle1.getPerimeter();

        System.out.println("Area rectangle = " + a1);
        System.out.println("Perimeter rectangle = " + p1);
        //System.out.println("Area rectangle = " + rectangle2.getArea());
        //System.out.println("Perimeter rectangle = " + rectangle2.getPerimeter());

        System.out.println("Area ellipse = " + e1);
        System.out.println("Perimeter ellipse = " + e2);

        System.out.println("Area circle = " + c1);
        System.out.println("Perimeter circle = " + c2);

        Figure square = new Square(5);
        Figure e8 = new Circle(1);

        System.out.println("Area square = " + square.getArea());
        System.out.println("Perimeter square = " + square.getPerimeter());

        square.setScale(3);

        System.out.println("Area square = " + square.getArea());
        System.out.println("Perimeter square = " + square.getPerimeter());
    }
}
