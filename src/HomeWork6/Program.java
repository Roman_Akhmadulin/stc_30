package HomeWork6;

public class Program {

    private String name;

    public Program(String name) {
        this.name = name;
    }

    public Program() {
    }

    @Override
    public String toString() {
        return "Program{" +
                "name='" + name + '\'' +
                '}';
    }
}
