package HomeWork3;

import java.util.Arrays;
import java.util.Scanner;

public class task1 {

    public static void sum() {
        Scanner scanner = new Scanner(System.in);
        int [] a = new int [scanner.nextInt()];
        int arraySum = 0;

        for (int i = 0; i < a.length; i++){
            a[i] = scanner.nextInt();
        }

        for (int i = 0; i < a.length; i++){
            arraySum = arraySum + a[i];
        }
        System.out.println("Сумма элементов массива: " + arraySum);
    }

    public static void swapInArray() {
        Scanner scanner = new Scanner(System.in);
        int [] n = new int[scanner.nextInt()];

        for (int i = 0; i < n.length; i++){
            n[i] = scanner.nextInt();
        }
        //System.out.println(Arrays.toString(n)); // Вывод массива на экран.

        for (int i = 0; i < n.length / 2; i++){
            int b = n[i];
            n[i] = n[n.length - 1 - i];
            n[n.length - i - 1] = b;
        }
        System.out.println("Обратная последовательность массива: " + Arrays.toString(n));
    }

    public static void arithmetic () {
        Scanner scanner = new Scanner(System.in);
        int [] a = new int[scanner.nextInt()];
        double arrayAverage = 0;

        for (int i = 0; i < a.length; i++){
            a[i] = scanner.nextInt();
        }

        for (int i = 0; i < a.length; i++){
            arrayAverage += a[i];
        }

        arrayAverage = arrayAverage / a.length;
        System.out.println("Среднеарифметическое число массива: " + arrayAverage);
    }

    public static void maxmin() {
        Scanner scanner = new Scanner(System.in);
        int [] a = new int[scanner.nextInt()];

        for (int i = 0; i < a.length; i++){
            a[i] = scanner.nextInt();
        }
        System.out.println(Arrays.toString(a));// Вывод введённого массива
        int minIndex = 0;
        int maxIndex = 0;
        // Поиск минимального и максимального значения в массиве
        for (int i = 1; i < a.length-1; i++){
            if(a[i]<minIndex) minIndex = a[i];
            if(a[i]>maxIndex) maxIndex = a[i];
        }
        //System.out.println("minIndex: " + minIndex + "; maxIndex: " + maxIndex);// Вывод минимального и максимального значения в массиве

        int temp = minIndex; // перестановка минимального индекса с максимальным
        minIndex = maxIndex;
        maxIndex = temp;

        System.out.println("minIndex: " + minIndex + "; maxIndex: " + maxIndex);
    }

    public static void bubble() {
        Scanner scanner = new Scanner(System.in);
        int[] a = new int[scanner.nextInt()];

        for (int i = 0; i < a.length; i++){
            a[i] = scanner.nextInt();
        }

        System.out.println(Arrays.toString(a)); // Вывод введённого массива
// Сравниваем элементы попарно
        for (int i = a.length - 1; i > 0; i--){
            for (int j = 0; j < i; j++){
                if (a[j] > a[j+1]){ // если они имеют неправильный порядок
                    int temp = a[j]; // то меняем местами
                    a[j] = a[j+1];
                    a[j+1] = temp;
                }
            }
        }
        System.out.println(Arrays.toString(a)); // Вывод отсортированного массива
    }

    public static void main(String[] args) {
        System.out.println("Введите размер массива ");
        sum();

        System.out.println("Введите размер массива ");
        swapInArray();

        System.out.println("Введите размер массива ");
        arithmetic();

        System.out.println("Введите размер массива ");
        maxmin();

        System.out.println("Введите размер массива ");
        bubble();
    }

}